<?php defined('_JEXEC') or die;

// variables
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$tpath = $this->baseurl . '/templates/' . $this->template;

// generator tag
$this->setGenerator(null);

?><!doctype html>

<html lang="<?php echo $this->language; ?>" class="b-offline-form__html">

<head>
	<jdoc:include type="head" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
</head>

<body class="b-offline-form__body">
<jdoc:include type="message" />
<div id="frame" class="b-offline-form">
	<?php if ($app->getCfg('offline_image')) : ?>
		<div class="b-offline-form__logo">
			<img src="<?php echo $app->getCfg('offline_image'); ?>" alt="<?php echo $app->getCfg('sitename'); ?>" />
		</div>
	<?php endif; ?>
	<h1 class="b-offline-form__title">
		<?php echo htmlspecialchars($app->getCfg('sitename')); ?>
	</h1>
	<?php if ($app->getCfg('display_offline_message', 1) == 1 && str_replace(' ', '', $app->getCfg('offline_message')) != ''): ?>
		<p><?php echo $app->getCfg('offline_message'); ?></p>
	<?php elseif ($app->getCfg('display_offline_message', 1) == 2 && str_replace(' ', '', JText::_('JOFFLINE_MESSAGE')) != ''): ?>
		<p><?php echo JText::_('JOFFLINE_MESSAGE'); ?></p>
	<?php endif; ?>
	<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" name="login" id="form-login" class="b-form">
		<div class="b-form__row" id="form-login-username">
			<label for="username"><?php echo JText::_('JGLOBAL_USERNAME'); ?></label><br />
			<input type="text" name="username" id="username" class="inputbox" alt="<?php echo JText::_('JGLOBAL_USERNAME'); ?>" size="18" />
		</div>
		<div class="b-form__row" id="form-login-password">
			<label for="passwd"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label><br />
			<input type="password" name="password" id="password" class="inputbox" alt="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" size="18" />
		</div>
		<div class="b-form__row" id="form-login-remember">
			<label for="remember">
				<input type="checkbox" name="remember" value="yes" alt="<?php echo JText::_('JGLOBAL_REMEMBER_ME'); ?>" id="remember" />
				<?php echo JText::_('JGLOBAL_REMEMBER_ME'); ?>
			</label>
		</div>
		<div class="b-form__row" id="form-login-submit">
			<label>&nbsp;</label>
			<input type="submit" name="Submit" class="b-btn big blue" value="<?php echo JText::_('JLOGIN'); ?>" />
		</div>
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.login" />
		<input type="hidden" name="return" value="<?php echo base64_encode(JURI::base()); ?>" />
		<?php echo JHTML::_('form.token'); ?>
	</form>
</div>
</body>

</html>