<?php defined('_JEXEC') or die;

include_once JPATH_THEMES . '/' . $this->template . '/libs/logic.php';

?><!doctype html>

<html lang="<?php echo $this->language; ?>">
<head>
	<jdoc:include type="head" />
</head>

<body
	class="<?php echo (($menu->getActive() == $menu->getDefault()) ? ('body-front') : ('body-site')) . ' ' . $active->alias . ' ' . $pageclass; ?>">
<?php if ($this->countModules('logo') || $this->countModules('headerbar')) : ?>
	<header class="header">
		<div class="wrapper">
			<?php if ($this->countModules('logo')) : ?>
				<a class="header__logo" href="<?php echo JURI::base(); ?>">
					<jdoc:include type="modules" name="logo" style="raw" />
				</a>
			<?php endif; ?>

			<?php if ($this->countModules('headerbar')) : ?>
				<div class="header__top">
					<jdoc:include type="modules" name="headerbar" style="raw" />
				</div>
			<?php endif; ?>
		</div>
	</header>
<?php endif; ?>
<?php if ($this->countModules('menu')) : ?>
	<a href="#" class="b-main-menu__toggle" data-mobile-menu="toggle">
		<span class="caret" aria-hidden="true"><i>&nbsp;</i></span>
		<span class="text"><?php echo JTEXT::_('MENU'); ?></span>
	</a>
	<nav class="b-main-menu">
		<jdoc:include type="modules" name="menu" style="raw" />
	</nav>
<?php endif; ?>
<jdoc:include type="message" />

<main class="b-main">
	<div class="wrapper">
		<jdoc:include type="component" />
	</div>
</main>
<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
