<?php defined('_JEXEC') or die;

// variables
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$tpath = $this->baseurl . '/templates/' . $this->template;

?><!doctype html>

<html class="b-page-error__html" lang="<?php echo $this->language; ?>">

<head>
	<title><?php echo $this->error->getCode(); ?> - <?php echo $this->title; ?></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
	<link rel="stylesheet" href="<?php echo $tpath; ?>/css/app.css?v=1">
</head>

<body class="b-page-error__body">
	<div class="b-page-error">
		<h1 class="b-page-error__title">
			<?php echo htmlspecialchars($app->getCfg('sitename')); ?>
		</h1>
		<div class="b-page-error__subtitle">
			<p>
				<?php
					echo $this->error->getCode() . ' - ' . $this->error->getMessage();
					if (($this->error->getCode()) == '404') {
						echo '<br />';
						echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND');
					}
				?>
			</p>
			<p>
				<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>:
				<a href="<?php echo $this->baseurl; ?>/"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a>.
			</p>
		</div>
	</div>
</body>

</html>
